﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class destroyAni : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "attackAni")//destroys animation
        {
            Destroy(other.gameObject);
            GameObject.Find("fireButton").GetComponent<onFireSelect>().aiTest();
            GameObject.Find("timeCounter").GetComponent<timeCountDown>().turnRestart = true;
            StartCoroutine("shieldFlicker");
           
        }
    }
    IEnumerator shieldFlicker()//makes shield flicker when hit with attack
    {
        audioManager.instance.PlaySoundEffect(audioManager.instance.shieldHit, this.gameObject.transform.position);
        this.gameObject.GetComponent<Image>().color = new Color(0.25f, 0.05f, 0, 0.45f);
        yield return new WaitForSeconds(0.15f);
        this.gameObject.GetComponent<Image>().color = new Color(1, 1, 1);
    }
}
