﻿using UnityEngine;
using System.Collections;

public class sceneManager : MonoBehaviour {//scene manager used for keeping track of data in scene
    public GameObject victoryBanner;
    public GameObject hullBreachBanner;
    public float returnToMainDelay;
    private int defenseChoice;
    private int attackChoice;
    private int player1Health;
    private int player2Health;
    public static sceneManager sceneInstance;
    void Awake()
    {
        if (sceneInstance == null)
        {
            sceneInstance = this;
            
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start()
    {

    }
	
	// Update is called once per frame
	void Update () {
      
        if (victoryBanner.activeInHierarchy == true || hullBreachBanner.activeInHierarchy == true)
        {
            StartCoroutine("timeToReturn");
        }
	}
    //get functions
    public int getDefenseChoice()
    {
        return defenseChoice;
    }
    public int getAttackChoice()
    {
        return attackChoice;
    }
    public int getPlayer1Health()
    {
        return player1Health;
    }
    public int getPlayer2Health()
    {
        return player2Health;
    }
    //set functions
    public void setDefenseChoice(int defenseOption)
    {
        defenseChoice = defenseOption;
    }
    public void setAttackChoice(int attackOption)
    {
        attackChoice = attackOption;
    }
    public void setPlayer1Health(int p1HP)
    {
        player1Health = p1HP;
    }
    public void setPlayer2Health(int p2HP)
    {
        player2Health = p2HP;
    }
    IEnumerator timeToReturn()
    {
        yield return new WaitForSeconds(returnToMainDelay);
        Application.LoadLevel("MainMenu");
    }
}
