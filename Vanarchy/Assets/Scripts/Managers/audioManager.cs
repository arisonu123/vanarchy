﻿using UnityEngine;
using System.Collections;

public class audioManager : MonoBehaviour {
    //Singleton Variable
    public static audioManager instance;

    //Public variables
    [Range(0.0f, 1.0f)]
    public float soundEffectsVolume;
    public AudioClip attack1;
    public AudioClip attack2;
    public AudioClip attack3;
    public AudioClip menuMusic;
    public AudioClip gameMusic;
    public AudioClip shieldHit;
    public AudioClip buttonClick;


    //Private variables
    private AudioSource playFrom;
    public AudioClip playSound;
    private float invokeDelay;
    private Vector3 sourcePosition;
    private Vector3 defaultVector3 = new Vector3(0, 0, 0);
    public string currentScene;
    public string previousScene;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
        previousScene = Application.loadedLevelName;
        currentScene = Application.loadedLevelName;
    }

    // Update is called once per frame
    void Update()
    {
        if (Application.loadedLevelName != currentScene)
        {
            previousScene = currentScene;
            currentScene = Application.loadedLevelName;
        }
        if (currentScene == "MainMenu"&&Input.GetKeyDown(KeyCode.Escape))//if player hits escape on main menu quit game
        {
            Application.Quit();
        }
        if (currentScene == "MainGame"&&this.gameObject.GetComponent<AudioSource>().clip!=gameMusic)
        {
            this.gameObject.GetComponent<AudioSource>().Stop();
            this.gameObject.GetComponent<AudioSource>().clip = gameMusic;
            this.gameObject.GetComponent<AudioSource>().Play();

        }
        if (currentScene == "MainMenu" && this.gameObject.GetComponent<AudioSource>().clip != menuMusic)
        {
            this.gameObject.GetComponent<AudioSource>().Stop();
            this.gameObject.GetComponent<AudioSource>().clip = menuMusic;
            this.gameObject.GetComponent<AudioSource>().Play();

        }
    }

    public void PlaySoundEffect(AudioClip audioSource, Vector2 position = new Vector2(), float delay = .1f)
    {
        playSound = audioSource;
        invokeDelay = delay;
        sourcePosition = position;
        Invoke("OneShotSoundEffect", invokeDelay);
    }

    private void OneShotSoundEffect()
    {
        if (playSound == buttonClick)
        {
            
                playFrom = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>();
                playFrom.Stop();
                playFrom.clip = playSound;
                playFrom.Play();
           
        }
        else if (playSound == shieldHit)
        {
            playFrom = GameObject.Find("Ship1").GetComponent<AudioSource>();
            playFrom.Stop();
            playFrom.clip = playSound;
            playFrom.Play();
        }
        else
        {
            playFrom = GameObject.Find("Ship2").GetComponent<AudioSource>();
            playFrom.Stop();
            playFrom.clip=playSound;
            if (playSound == attack3)
            {
                playFrom.volume = .5f;
            }
            else
            {
                playFrom.volume = 1f;
            }
            playFrom.Play();
                
            
        }
    }
}
