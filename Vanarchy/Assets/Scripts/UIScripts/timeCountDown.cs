﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class timeCountDown : MonoBehaviour {
    private Text timerText;
    private float counter;
    public int maxTimer;
    private int currentTimer;
    public bool turnRestart = false;
    public bool turnPause = false;
	
	void Start () {
        timerText = gameObject.GetComponent<Text>();
        counter = currentTimer = maxTimer;
	}
	
	
	void Update () {
        counter -= Time.deltaTime;

        if (turnPause == false && currentTimer!= 0 && counter <= (currentTimer-1))
        {
            currentTimer--;
            timerText.text = currentTimer.ToString();
        }
        if (counter < 0 && currentTimer == 0)//out of time,use current selections
        {
            currentTimer = -1;
            GameObject.Find("fireButton").GetComponent<onFireSelect>().outOfTime();
        }
       if (turnRestart == true)
       {
            counter = currentTimer = maxTimer;
            timerText.text = currentTimer.ToString();
            turnRestart = false;
            turnPause = false;
       }
    }
}
