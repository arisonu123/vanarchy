﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;



public class inputActions : MonoBehaviour {
    public InputField user;
    public InputField pass;
    private fileMod newFileCheck;
    // Use this for initialization

    public void Start()
    {
        //Adds a listener that invokes the "LockInput" method when the player finishes editing the main input field.
        //Passes the main input field into the method when "LockInput" is invoked
        user.onEndEdit.AddListener(delegate { finishedTypingUser(user); });
        pass.onEndEdit.AddListener(delegate { finishedTypingPass(pass); });
    }


    // Update is called once per frame
    void Update()
    {

    }
    public void finishedTypingUser(InputField username)
    {
        if (username.text.Length > 0)
        {
            GameMaster.instance.currentUser = username.text;
        }
        else if (username.text.Length == 0)
        {
            Debug.Log("Main Input Empty");
        }
    }
    public void finishedTypingPass(InputField password)
    {
        if (password.text.Length > 0)
        {
            GameMaster.instance.currentPass = password.text;
        }
        else if (password.text.Length == 0)
        {
            Debug.Log("Main Input Empty");
        }
    }
    public void login()
    {
        newFileCheck = new fileMod();
        newFileCheck.loginAttempt(GameMaster.instance.currentUser, GameMaster.instance.currentPass);
        if (newFileCheck.accountSuccess == true)
        {
            Application.LoadLevel("MainGame");
        }

    }
    public void createAccount()
    {
        newFileCheck = new fileMod();
        newFileCheck.searchAccounts(GameMaster.instance.currentUser);
        if (newFileCheck.accountExists)
        {
            Debug.Log("Account exists, failed to make");
        }
        else
        {
            Debug.Log("Account made");
        }
       
    }
	
}
