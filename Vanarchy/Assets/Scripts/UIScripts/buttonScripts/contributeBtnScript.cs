﻿using UnityEngine;
using System.Collections;

public class contributeBtnScript : ButtonBase
{
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    public void OnMouseEnterLink()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }
    public void OnMouseExitLink()
    {
        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    }
    public override void Pressed()//opens up paypal donation when pressed
    {
        audioManager.instance.PlaySoundEffect(audioManager.instance.buttonClick, this.gameObject.transform.position,0);
        Application.OpenURL("http://www.vahaos.com/donate.php");
    }

}
