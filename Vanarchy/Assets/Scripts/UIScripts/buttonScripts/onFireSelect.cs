﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class onFireSelect : ButtonBase {
    public Sprite pressedImage;
    public Sprite unpressedImage;
    public GameObject laserAttack;
    public GameObject cannonAttack;
    public GameObject missileAttack;
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    public float counter = 0;
    private int defenseSelection;
    private int attackSelection;
    private int defenseSelect2;
    private int attackSelect2;
    private setHealth player1HealthScript, player2HealthScript;
    private Image fireButtonImage;
	// Use this for initialization
	void Start () {
        player1HealthScript = GameObject.Find("health1").GetComponent<setHealth>();
        player2HealthScript = GameObject.Find("health2").GetComponent<setHealth>();
        fireButtonImage = this.gameObject.GetComponent<Image>();
    }
	
	// Update is called once per frame
	void Update () {
        counter += Time.deltaTime;
    }
   
    public void OnMouseEnterLink()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }
    public void OnMouseExitLink()
    {
        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    }

    public override void Pressed()
    {

        if (sceneManager.sceneInstance.victoryBanner.activeInHierarchy == false && sceneManager.sceneInstance.hullBreachBanner.activeInHierarchy == false)
        {
            if (counter>1 && GameObject.Find("timeCounter").GetComponent<timeCountDown>().turnPause == false)
           {
                GameObject.Find("timeCounter").GetComponent<timeCountDown>().turnPause = true;
                fireButtonImage.sprite = pressedImage;
                audioManager.instance.PlaySoundEffect(audioManager.instance.buttonClick, this.gameObject.transform.position);
                defenseSelection = sceneManager.sceneInstance.getDefenseChoice();
                attackSelection = sceneManager.sceneInstance.getAttackChoice();
                if (attackSelection == 0)
                {
                    GameObject attack = Instantiate(laserAttack, new Vector3(40, 10, 80), Quaternion.identity) as GameObject;

                    audioManager.instance.PlaySoundEffect(audioManager.instance.attack1, GameObject.Find("Ship2").transform.position);
                }
                if (attackSelection == 1)
                {
                    GameObject attack = Instantiate(cannonAttack, new Vector3(40, 10, 80), Quaternion.identity) as GameObject;

                    audioManager.instance.PlaySoundEffect(audioManager.instance.attack2, GameObject.Find("Ship2").transform.position);
                }
                if (attackSelection == 2)
                {
                    GameObject attack = Instantiate(missileAttack, new Vector3(40, 10, 80), Quaternion.identity) as GameObject;
                    audioManager.instance.PlaySoundEffect(audioManager.instance.attack3, GameObject.Find("Ship2").transform.position);
                }
                Debug.Log(attackSelection + "is attack.");
                // aiTest();
                counter = 0;
            }
        }


    }
    public void Released()
    {
        fireButtonImage.sprite = unpressedImage;
    }
    public void outOfTime()//if player runs out of time commence turn with current attacks
    {
        if (sceneManager.sceneInstance.victoryBanner.activeInHierarchy == false && sceneManager.sceneInstance.hullBreachBanner.activeInHierarchy == false)
        {
            GameObject.Find("timeCounter").GetComponent<timeCountDown>().turnPause = true;
            defenseSelection = sceneManager.sceneInstance.getDefenseChoice();
            attackSelection = sceneManager.sceneInstance.getAttackChoice();
            if (attackSelection == 0)
            {
                GameObject attack = Instantiate(laserAttack, new Vector3(40, 10, 80), Quaternion.identity) as GameObject;

                audioManager.instance.PlaySoundEffect(audioManager.instance.attack1, GameObject.Find("Ship2").transform.position);
            }
            if (attackSelection == 1)
            {
                GameObject attack = Instantiate(cannonAttack, new Vector3(40, 10, 80), Quaternion.identity) as GameObject;

                audioManager.instance.PlaySoundEffect(audioManager.instance.attack2, GameObject.Find("Ship2").transform.position);
            }
            if (attackSelection == 2)
            {
                GameObject attack = Instantiate(missileAttack, new Vector3(40, 10, 80), Quaternion.identity) as GameObject;
                audioManager.instance.PlaySoundEffect(audioManager.instance.attack3, GameObject.Find("Ship2").transform.position);
            }
            Debug.Log(attackSelection + "is attack.");
            //aiTest();
        }
    }
    public void aiTest()
    {
        defenseSelect2 = Random.Range(0,2);
        attackSelect2 = Random.Range(0, 2);
        damageStep(defenseSelection, attackSelection, defenseSelect2, attackSelect2);
      
    }
    public void damageStep(int defense1,int attack1, int defense2,int attack2)
    {

        if (attack1 != defense2)
        {
            player2HealthScript.tookDamage = true;
        }
        if (attack2 != defense1)
        {
            player1HealthScript.tookDamage = true;
        }
        
    }
}
