﻿using UnityEngine;
using System.Collections;

/**************************************************************************************************
  Generic button class, all buttons will inherit from this class for use of the 'pressed' function
**************************************************************************************************/
public class ButtonBase : MonoBehaviour {

    public virtual void Pressed() { }
}
