﻿using UnityEngine;
using System.Collections;

public class UserInputControls : MonoBehaviour
{

    void Start() { }

    void Update()
    {
        if (Input.GetMouseButtonDown(0)) // Mouse controls
        //if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began) // Touch controls
        {
            //RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position), Vector2.zero); // Touch controls
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider) // We touched something with a collider
            {
                if (hit.collider.tag == "Button")
                {
                    // Call button script, have it do whatever the button needs to do
                    hit.collider.gameObject.GetComponent<ButtonBase>().Pressed();
                }

            } // - collider

        } // - Button Down / Screen Touched

    } // - Update

}
