﻿using UnityEngine;
using System.Collections;

public class SystemsButtonScript : ButtonBase
{
    public GameObject surrenderButton;
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    public void OnMouseEnterLink()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }
    public void OnMouseExitLink()
    {
        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    }
    public override void Pressed()
    {
        if (sceneManager.sceneInstance.victoryBanner.activeInHierarchy == false && sceneManager.sceneInstance.hullBreachBanner.activeInHierarchy == false)
        {
            audioManager.instance.PlaySoundEffect(audioManager.instance.buttonClick, this.gameObject.transform.position);
            if (surrenderButton.activeInHierarchy == false)
            {
                surrenderButton.SetActive(true);
            }
            else
            {
                surrenderButton.SetActive(false);
            }
        }
    }
}
