﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Defense_Base : ButtonBase{
    public int DefenseType=1; // 0, 1, 2 for the three different attack and defense types - Keep it simple.
    public Sprite shieldLaser,shieldCannon,shieldMissle;
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    private Image BtnImage; // The image for this button
    

    void Start()
    {
        BtnImage = gameObject.GetComponent<Image>();
        
    }
    public void OnMouseEnterLink()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }
    public void OnMouseExitLink()
    {
        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    }
    public override void Pressed()
    {
        if (sceneManager.sceneInstance.victoryBanner.activeInHierarchy == false && sceneManager.sceneInstance.hullBreachBanner.activeInHierarchy == false)
        {
            if (GameObject.Find("timeCounter").GetComponent<timeCountDown>().turnPause == false)
            {
                audioManager.instance.PlaySoundEffect(audioManager.instance.buttonClick, this.gameObject.transform.position);
                toggleOtherDefenseOptions();
            }
        }
    }

    // Toggles other defense options
    void toggleOtherDefenseOptions()
    {
        if (DefenseType == 0)
        {
            BtnImage.sprite = shieldCannon;
            DefenseType = 1;
         

        }

        else if (DefenseType == 1){
            BtnImage.sprite = shieldMissle;
            DefenseType = 2;
        }
        else if (DefenseType == 2)
        {
            BtnImage.sprite = shieldLaser;
            DefenseType = 0;
        }
        sceneManager.sceneInstance.setDefenseChoice(DefenseType);//set defense choice in sceneManager

    }
}
