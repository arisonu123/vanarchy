﻿using UnityEngine;
using System.Collections;

public class nameSiteLinks:ButtonBase {
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    public void OnMouseEnterLink()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }
    public void OnMouseExitLink()
    {
        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    }

    public void coryLink()
    {
        Application.OpenURL("http:\\www.vahaos.com");
    }
    public void emilyLink()
    {
        Application.OpenURL("http:\\www.emilystepp.com");
    }
    public void shaunLink()
    {
        Application.OpenURL("https://www.linkedin.com/in/snorem");
    }
    public void alisonLink()
    {
        Application.OpenURL("https://alisontaylorprojects.wordpress.com/");
    }
}

