﻿using UnityEngine;
using System.Collections;

public class EngageBtnScript : ButtonBase
{
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    public void OnMouseEnterLink()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }
    public void OnMouseExitLink()
    {
        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    }
    // Will need to bring player to "Finding opponent" screen, a screen before the
    // actual gameplay can commence.

    // Temporarily settup to bring to main game screen
    public void clickSound()//
    {
        audioManager.instance.PlaySoundEffect(audioManager.instance.buttonClick, this.gameObject.transform.position, 0);
    }
    public override void Pressed()
    {
        Application.LoadLevel("MainGame");

    }
}