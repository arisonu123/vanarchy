﻿using UnityEngine;
using System.Collections;

public class AssistanceBtnScript : ButtonBase {
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    public void OnMouseEnterLink()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }
    public void OnMouseExitLink()
    {
        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    }
    public void clickSound()//
    {
        audioManager.instance.PlaySoundEffect(audioManager.instance.buttonClick, this.gameObject.transform.position, 0);
    }
    public override void Pressed()//load help screen
    { 
        Application.LoadLevel("helpDiagram");
    }

    }


