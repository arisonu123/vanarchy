﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AttackBase : ButtonBase {
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    public int AttackType; // 0, 1, 2 for the three different attack and defense types - Keep it simple.
    public Sprite Unselected, Selected;
   public Image BtnImage; // The image for this button
    private AttackBase OtherWeapon1, OtherWeapon2; // The scripts for the other weapon objects

    void Start()
    {
        findOtherWeapons();
        BtnImage = gameObject.GetComponent<Image>();
        if (AttackType == 0) BtnImage.sprite = Selected;
        else BtnImage.sprite = Unselected;
    }
    public void OnMouseEnterLink()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }
    public void OnMouseExitLink()
    {
        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    }
    public override void Pressed()
    {
        if (sceneManager.sceneInstance.victoryBanner.activeInHierarchy == false && sceneManager.sceneInstance.hullBreachBanner.activeInHierarchy == false)
        {
            if (GameObject.Find("timeCounter").GetComponent<timeCountDown>().turnPause == false)
            {
                if (BtnImage.sprite != Selected)
                {
                    audioManager.instance.PlaySoundEffect(audioManager.instance.buttonClick, this.gameObject.transform.position);
                    toggleOtherWeapons();
                    toggleWeaponOn();
                }
            }
        }
    }
   
    // Toggles other weapon options
    void toggleOtherWeapons()
    {
        sceneManager.sceneInstance.setAttackChoice(AttackType);//set attack choice in sceneManager
        OtherWeapon1.toggleWeaponOff();
        OtherWeapon2.toggleWeaponOff();
    }

    // Toggles this weapon
    void toggleWeaponOn()
    {
        BtnImage.sprite = Selected;
    }

    // Toggles this weapon off
    public void toggleWeaponOff()
    {
        if(BtnImage.sprite == Selected) BtnImage.sprite = Unselected;
    }

    // Finds the other weapon objects
    void findOtherWeapons()
    {
        if (gameObject.name == "laserButton")
        {
            OtherWeapon1 = GameObject.Find("cannonButton").GetComponent<AttackBase>();
            OtherWeapon2 = GameObject.Find("missileButton").GetComponent<AttackBase>();
           
        }
        else if (gameObject.name == "cannonButton")
        {
            OtherWeapon1 = GameObject.Find("laserButton").GetComponent<AttackBase>();
            OtherWeapon2 = GameObject.Find("missileButton").GetComponent<AttackBase>();
           
        }
        else // missileButton
        {
            OtherWeapon1 = GameObject.Find("laserButton").GetComponent<AttackBase>();
            OtherWeapon2 = GameObject.Find("cannonButton").GetComponent<AttackBase>();
        }
    }
}
