﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class setHealth : MonoBehaviour {//used to set health text and corresponding deteriorating ship look
    public Sprite[] healthNums;
    public Sprite[] shipLooks;
    public int health = 5;
    public bool tookDamage = false;
    public GameObject[] endBanners;
    private Image healthImage;
    private GameObject ship;
    private Image shipImage;
    public enum player { player1, player2};
    public player currentPlayer;
	// Use this for initialization
	void Start () {
        healthImage = this.gameObject.GetComponent<Image>();
        if (this.gameObject.name == "health2")//if your health get your ship picture
        {
            ship = GameObject.Find("Ship2");
        }
        if (this.gameObject.name == "health1")//other player
        {
            ship = GameObject.Find("Ship1");
        }
        shipImage = ship.GetComponent<Image>();
        
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.P))//test code
        {
            health = 0;
            sceneManager.sceneInstance.setPlayer1Health(0);
            sceneManager.sceneInstance.setPlayer2Health(0);
            healthImage.sprite = healthNums[health];
            shipImage.sprite = shipLooks[health];
            if (sceneManager.sceneInstance.getPlayer1Health() == 0 && sceneManager.sceneInstance.getPlayer2Health() == 0)//tie
            {
                endBanners[1].SetActive(true);
                GameObject.Find("timeCounter").GetComponent<timeCountDown>().enabled = false;
            }
            else if (health == 0)//if your health equals 0 loss
            {
                endBanners[1].SetActive(true);
                GameObject.Find("timeCounter").GetComponent<timeCountDown>().enabled = false;
            }
        }
        if (tookDamage == true&&GameObject.Find("health1").GetComponent<setHealth>().health!=0&& GameObject.Find("health2").GetComponent<setHealth>().health!=0)//if ship takes damage then change health percent and ship look
        {
            health -= 1;
            healthImage.sprite = healthNums[health];
            shipImage.sprite = shipLooks[health];
            if (currentPlayer == player.player1)
            {
                sceneManager.sceneInstance.setPlayer1Health(health);
                if (sceneManager.sceneInstance.getPlayer1Health()==0 && sceneManager.sceneInstance.getPlayer2Health()==0)//tie
                {
                    endBanners[1].SetActive(true);
                    GameObject.Find("timeCounter").GetComponent<timeCountDown>().enabled = false;
                   
                }
                else if (health==0)//if your health equals 0 loss
                {
                    endBanners[1].SetActive(true);
                    GameObject.Find("timeCounter").GetComponent<timeCountDown>().enabled = false;
                   
                }
            }
            if (currentPlayer == player.player2)//may not be needed
            {
                sceneManager.sceneInstance.setPlayer2Health(health);
                if (sceneManager.sceneInstance.getPlayer1Health() == 0 && sceneManager.sceneInstance.getPlayer2Health() == 0)//tie
                {
                    endBanners[1].SetActive(true);
                    GameObject.Find("timeCounter").GetComponent<timeCountDown>().enabled = false;
                    GameObject[] buttons = GameObject.FindGameObjectsWithTag("Button");
                    foreach (GameObject btn in buttons)
                    {
                        if (btn.GetComponent<ButtonBase>() != null)
                        {
                            btn.GetComponent<ButtonBase>().enabled = false;
                        }
                    }
                }
                else if (health == 0)//if enemy's health equals 0 victory
                {
                    endBanners[0].SetActive(true);
                    GameObject.Find("timeCounter").GetComponent<timeCountDown>().enabled = false;
                    GameObject[] buttons = GameObject.FindGameObjectsWithTag("Button");
                    foreach (GameObject btn in buttons)
                    {
                        if (btn.GetComponent<ButtonBase>() != null)
                        {
                            btn.GetComponent<ButtonBase>().enabled = false;
                        }
                    }
                }
            }
            tookDamage = false;
        }
        if (tookDamage == false && health == 0 && sceneManager.sceneInstance.victoryBanner.activeInHierarchy == false && sceneManager.sceneInstance.victoryBanner.activeInHierarchy == false)
        {//if player surrenders, end game
            healthImage.sprite = healthNums[health];
            shipImage.sprite = shipLooks[health];
            sceneManager.sceneInstance.setPlayer1Health(health);
            endBanners[1].SetActive(true);
            GameObject.Find("timeCounter").GetComponent<timeCountDown>().enabled = false;
        }
	}
}
