﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;
public class fileMod
{
    private const string FILE_NAME = "vanarchyAccounts.txt";
    private Dictionary<string, string> userData =
        new Dictionary<string, string>();
    private bool start = false;
    public bool accountExists;
    public bool accountSuccess;
    public static void Main(String[] args)
    {
        fileMod fileModification = new fileMod();
            fileModification.startUp();
        
     
    }
    public void startUp()
    {
        if (start == false)
        {
            StreamReader sr;
            if (File.Exists(FILE_NAME))
            {
                string line;
                sr = new StreamReader(FILE_NAME);
                while ((line = sr.ReadLine()) != null)
                {
                    string[] userInfo = line.Split(',');
                    appendUserData(userInfo[0], userInfo[1]);//add users to game client
                    Debug.Log("user:" + userInfo[0] + " pass: " + userInfo[1]);
                }
                start = true;
                return;
            }
            
        }
    }
    public  void appendUserData(string user,string pass)//add new user
    {
        userData.Add(user, pass);
    }
    public void addAccount(string user)//add accounts if they do not exist
    {
        StreamWriter sr;
        string password;
        if (File.Exists(FILE_NAME))//if file exists append to file
        {
            using (StreamWriter file = File.AppendText(FILE_NAME))
            {

                userData.TryGetValue(user, out password);
                file.WriteLine(user + "," + password);
                file.Close();
            }
            return;
        }
        else//else create file
        {
            sr = File.CreateText(FILE_NAME);
            userData.TryGetValue(user, out password);
            sr.WriteLine(user + "," + password);
            sr.Close();
        }
    }

    public void searchAccounts(string user)//check if account exists for purpose of creating accounts
    {
        if (userData.ContainsKey(user)==true)//if user exists do not make account
        {
            accountExists = true;
        }
        else if(userData.ContainsKey(user)==false)//account does not exist, add account
        {
            appendUserData(user, GameMaster.instance.currentPass);
            addAccount(user);
        }
    }

    public void loginAttempt(string user,string password)
    {
        string attempt = "";
        if (userData.ContainsKey(user))
        {
             userData.TryGetValue(user, out attempt);
            if (password == attempt)
            {
                accountSuccess = true;
            }
            else
            {
                accountSuccess = false;
            }
        }
        else
        {
            accountSuccess = false;
        }
        Debug.Log("login" + accountSuccess);
    }
}