﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TitleScript : MonoBehaviour {

    public float SceneTimer;
    private float fade;
    private Image Transparency;

	void Start () {
        Transparency = GameObject.Find("TitleText").GetComponent<Image>();
	}
	
	void Update () {
       fade = Time.time / SceneTimer;
       if (fade > 1.0f) fade = 1.0f;
       Transparency.color = new Color(1f, 1f, 1f, Mathf.SmoothStep(0.0f, 1f, fade));

        if (Time.time >= SceneTimer + 2.0f) loadMenu();
	}

    void loadMenu()
    {
        Application.LoadLevel("MainMenu");
    }
}
